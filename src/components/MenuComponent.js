import React from 'react';
import {Card, CardImg, CardImgOverlay, CardTitle, Breadcrumb, BreadcrumbItem} from 'reactstrap';
import {Link} from 'react-router-dom'; 
import { Loading } from './LoadingComponent';
import {baseURL} from '../shared/baseURL';

function RenderItem(dish){
    return(
        <Card>
            <Link to={`/menu/${dish.id}`}>
                <CardImg src={baseURL + dish.image} alt={dish.image} height="400" width="250"></CardImg>
                <CardImgOverlay>
                    <CardTitle>{dish.name}</CardTitle>
                </CardImgOverlay>
            </Link>
        </Card>
    )
}

const Menu = (props) => {

    const menu = props.dish.dishesState.map((dish) =>{
        return(
            <div key={dish.id} className="col-12 col-md-5 m-1">
                {RenderItem(dish)}
            </div>
        );
    });

    if (props.dish.isLoading) 
    {
        return(
            <div className="container">
                <div className="row">            
                    <Loading />
                </div>
            </div>
        );
    }
    else if (props.dish.errMess) 
    {
        return(
            <div className="container">
                <div className="row"> 
                    <div className="col-12">
                        <h4>{props.dish.errMess}</h4>
                    </div>
                </div>
            </div>
        );
    }
    else
    {
        return(
            <div className="container">
                <div className="row">
                    <Breadcrumb>
                        <BreadcrumbItem><Link to="/home">Home</Link></BreadcrumbItem>
                        <BreadcrumbItem active>Menu</BreadcrumbItem>
                    </Breadcrumb>
                    <div className="col-12">
                        <h3>Menu</h3>
                    </div>
                </div>
                <div className="row row-content-border">
                    {menu}
                </div>
            </div>
        );
    }
}

export default Menu;