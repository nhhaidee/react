import React, { Component } from 'react';
import {CardImg, CardTitle, Card, CardBody, CardText, Breadcrumb, BreadcrumbItem, 
        Button, Modal, ModalHeader, ModalBody, Label, Row, Col} from 'reactstrap';
import {Link} from 'react-router-dom';
import {Control, LocalForm, Errors} from 'react-redux-form';
import { Loading } from './LoadingComponent';
import {baseURL} from '../shared/baseURL';

const required = (val) => val && val.length;
const maxLength = (len) => (val) => !(val) || (val.length <= len);
const minLength = (len) => (val) => val && (val.length >= len);

function  RenderDish(dish){
    if(dish != null){
        return(
            <div className="col-12 col-md-5 m-1">
                <Card>
                    <CardImg top src={baseURL + dish.image} alt={dish.image}></CardImg>
                    <CardBody>
                        <CardTitle>{dish.name}</CardTitle>
                        <CardText>{dish.description}</CardText>
                    </CardBody>
                </Card>
            </div>
        );
    }
    else{
        return(
            <div></div>
        );
    }
}

function  RenderComments(dishComments, postComment, dishId) {

    const listComments = dishComments.map((dishComments) => {
        return (
            <li key={dishComments.id}>
                <p>{dishComments.comment}</p>
                <p>-- {dishComments.author},
                &nbsp;
                {new Intl.DateTimeFormat('en-US', {
                        year: 'numeric',
                        month: 'long',
                        day: '2-digit'
                    }).format(new Date(dishComments.date))}
                </p>
            </li>
        );
    });

    return (
        <div className="col-12 col-md-5 m-1">
            <h4> Comments </h4>
            <ul className="list-unstyled">
                {listComments}
            </ul>
            <CommentForm dishId={dishId} postComment={postComment}/>
        </div>
    ); 

}

const DishDetail = (props) =>{

    if (props.isLoading)
    {
        return(
            <div className="container">
                <div className="row">
                    <Loading />
                </div>
            </div>
        )
    }
    else if (props.errMess)
    {
        return(
            <div className="container">
                <div className="row">
                    <h4>{props.errMess}</h4>
                </div>
            </div>
        )
    }
    if (props.dish == null){
        return(<div></div>);
    }
    else{
        return(
            <div className="container">
                <div className="row">
                    <Breadcrumb>
                        <BreadcrumbItem><Link to="/home">Home</Link></BreadcrumbItem>
                        <BreadcrumbItem><Link to="/menu">Menu</Link></BreadcrumbItem>
                        <BreadcrumbItem>{props.dish.name}</BreadcrumbItem>
                    </Breadcrumb>
                    <div className="col-12">
                        <h3>{props.dish.name}</h3>
                    </div>
                </div>

                <div className="row row-content-border">
                    {RenderDish(props.dish)}
                    {RenderComments(props.comment, props.postComment, props.dish.id)}
                </div>
            </div>
        );
    }
}

class CommentForm extends Component{

    constructor(props){
        super(props);
        this.state={
            isCommentModalOpen: false
        }
        this.toggleCommentModal = this.toggleCommentModal.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleCommentModal(){
        this.setState({
            isCommentModalOpen:!this.state.isCommentModalOpen
        });
    }

    handleSubmit(values){
        this.toggleCommentModal();
        this.props.postComment(this.props.dishId, values.rating, values.author, values.comment);
    }
    render(){
        return(
            <div>
                <Button outline onClick={this.toggleCommentModal}><span className="fa fa-edit fa-lg"></span>Submit Comment</Button>
                <Modal isOpen={this.state.isCommentModalOpen} toggle={this.toggleCommentModal}>
                    <ModalHeader toggle={this.toggleCommentModal}>Submit Comment</ModalHeader>
                    <ModalBody>
                        <LocalForm onSubmit={(values) => this.handleSubmit(values)}>
                            <Row className="form-group">
                                <Label htmlFor="rating" md={12}>Rating</Label>
                                <Col md={12}>
                                    <Control.select model=".rating" defaultValue="1" className="form-control" id="rating" name="rating">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                    </Control.select>
                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Label htmlFor="author" md={12}>Your Name</Label>
                                <Col md={12}>
                                    <Control.text model=".author" className="form-control" id="author" name="author"
                                    validators={{ required, minLength: minLength(3), maxLength: maxLength(15) }}
                                    ></Control.text>
                                    <Errors className="text-danger" model=".author" show="touched" 
                                            messages={{ required: "Required and ", 
                                            minLength: "Must be greater than 3 characters", 
                                            maxLength: "Must be 15 charaters or less" }} />
                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Label htmlFor="comment" md={12}>Comment</Label>
                                <Col md={12}>
                                    <Control.textarea model=".comment" rows={6} className="form-control" id="comment" name="comment"
                                       validators={{required}}></Control.textarea>
                                    <Errors className="text-danger" model=".comment" show="touched" messages={{required:"Please give your comments"}}></Errors>
                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Col md={12}>
                                    <Button type="submit" color="primary">Submit</Button>{' '}
                                    <Button type="button" color="secondary" onClick={this.toggleCommentModal}>Cancel</Button>
                                </Col>
                            </Row>
                        </LocalForm>
                    </ModalBody>
                </Modal>
            </div>
        );
    }
}
export default DishDetail;