import React from 'react';
import { Card, CardImg, CardText, CardBody,
         CardTitle, CardSubtitle,Breadcrumb, BreadcrumbItem} from 'reactstrap';
import {Link} from 'react-router-dom';
import {Loading} from './LoadingComponent';
import {baseURL} from '../shared/baseURL';

function RenderCard(item, isLoading, errMess) {

    if (isLoading) 
    {
        return(
            <Loading />
        );
    }
    else if (errMess) 
    {
        return(
            <h4>{errMess}</h4>
        );
    }
    else
    {
        return(
            <Card>
                <CardImg src={baseURL + item.image} alt={item.image}></CardImg>
                <CardBody>
                    <CardTitle>{item.name}</CardTitle>
                    {
                        item.designation ? <CardSubtitle>{item.designation}</CardSubtitle> : null
                    }
                    <CardText>{item.description}</CardText>
                </CardBody>
            </Card>
        );
    }
}

function Home(props) {
    return(
        <div className="container">
            <div className="row">
                <div className="col-12">
                    <Breadcrumb>
                        <BreadcrumbItem><Link to ="">Home</Link></BreadcrumbItem>
                    </Breadcrumb>
                </div>
                <div className="col-12 col-md m-1">
                    {RenderCard(props.dish, props.dishesLoading, props.dishesErrMess)}
                </div>
                <div className="col-12 col-md m-1">
                    {RenderCard(props.promotion, props.promoLoading, props.promoErrMess)}
                </div>
                <div className="col-12 col-md m-1">
                    {RenderCard(props.leader, props.leaderLoading, props.leaderErrMess)}
                </div>
            </div>
        </div>
    );
}

export default Home;