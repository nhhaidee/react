import React, { Component } from 'react';
import Home from './HomeComponent';
import Menu from './MenuComponent';
import About from './AboutComponent';
import DishDetail from './DishDetailComponent';
import Contact from './ContactComponent';
import Header from './HeaderComponent';
import Footer from './FooterComponent';
import {Switch, Route, Redirect, withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {postCommentCreators, postFeedbackCreators, fetchDishesCreators, fetchPromosCreators, fetchCommentsCreator, fetchLeadersCreators} from '../redux/actions/ActionCreators';
import {actions} from 'react-redux-form';



const mapStateToProps = state => {
  return {
    maindishes : state.dishesStore,
    maincomments: state.commentsStore,
    mainpromotions: state.promotionsStore,
    mainleaders: state.leadersStore
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    postComment:(dishId, rating, author, comment) => dispatch(postCommentCreators(dishId, rating, author, comment)),
    fetchDishes: () => dispatch(fetchDishesCreators()),
    fetchPromos: () => dispatch(fetchPromosCreators()),
    fetchComments:() => dispatch(fetchCommentsCreator()),
    fetchLeaders:() => dispatch(fetchLeadersCreators()),
    postFeebBack:(values) => postFeedbackCreators(values),
    resetFeedbackForm: () => dispatch(actions.reset('feedback'))
  };
};

class Main extends Component {
  
  constructor(props)
  {
    super(props);    
  }

  componentDidMount()
  {
    this.props.fetchDishes();
    this.props.fetchPromos();
    this.props.fetchComments();
    this.props.fetchLeaders();
  }


  render()
  {
    const HomePage = () =>{
        return(
          <Home dish={this.props.maindishes.dishesState.filter((dish) => dish.featured)[0]}
                dishesLoading = {this.props.maindishes.isLoading}
                dishesErrMess = {this.props.maindishes.errMess}
                promotion={this.props.mainpromotions.promotionsState.filter((promo) => promo.featured)[0]} 
                promoLoading={this.props.mainpromotions.isLoading}
                promoErrMess={this.props.mainpromotions.errMess}
                leader={this.props.mainleaders.leadersState.filter((leader) => leader.featured)[0]}
                leaderLoading={this.props.mainleaders.isLoading}
                leaderErrMess={this.props.mainleaders.errMess}
          ></Home>
        );
    };
  
    const DishWithId = ({match}) => {
      return(
        <DishDetail dish={this.props.maindishes.dishesState.filter((dish) => dish.id === parseInt(match.params.dishMatchId,10))[0]} 
                    isLoading = {this.props.maindishes.isLoading}
                    errMess = {this.props.maindishes.errMess}
                    comment={this.props.maincomments.commentsState.filter((comment) => comment.dishId === parseInt(match.params.dishMatchId,10))}
                    commenstErrMess ={this.props.maincomments.errMess} 
                    postComment ={this.props.postComment}/>
      );
    };

    return(
        <div>
          <Header></Header>
          <Switch>
            <Route path="/home"  component={HomePage}></Route>
            <Route exact path="/menu" component={() => <Menu dish={this.props.maindishes}></Menu>}></Route>
            <Route path="/menu/:dishMatchId" component={DishWithId}></Route>
            <Route exact path="/aboutus" component={() => <About leaders={this.props.mainleaders.leadersState}></About>}></Route>
            <Route exact path="/contactus" component={() => <Contact resetFeedbackForm={this.props.resetFeedbackForm}
                                                                    postFeedBack={this.props.postFeebBack}></Contact>}></Route>
            <Redirect to="/home"></Redirect>
          </Switch>
          <Footer></Footer>
        </div>
    );
  }
}

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(Main));