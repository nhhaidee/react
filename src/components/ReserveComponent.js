import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody,
       Form, FormGroup, Label, Input} from 'reactstrap';
// or const Resever = () =>
function Reserve () {

  const [modal, setModal] = useState(false); // using Hooks

  const toggle = () => setModal(!modal);

  return (
    <div>
      <Button color="warning" onClick={toggle} size="lg">Reserve a Table</Button>
      <Modal isOpen={modal} toggle={toggle}>
        <ModalHeader toggle={toggle}>Reserve a Table</ModalHeader>
        <ModalBody>
          <Form>
            <FormGroup>
              <Label for="NoGuest">Number of Guests</Label>
              <Input type="select" name="select" id="NoGuest">
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                <option>6</option>
              </Input>
            </FormGroup>
            <FormGroup>
              <Label for="dateandtime" class="col-12 offset-sm-1 col-sm-2">Date and Time</Label>
              <Input type="date" class="form-control" id="date" placeholder="Date" />
              <Input type="time" class="form-control" id="time" placeholder="Time" />
            </FormGroup>
            <FormGroup>
                <Label for="name">Name</Label>
                <Input type="name" name="name" id="name" placeholder="Name" />
              </FormGroup>
              <FormGroup>
                <Label for="phonenumber">Tel No</Label>
                <Input type="phone" name="phone" id="phone" placeholder="Telephone Number" />
            </FormGroup>
            <FormGroup>
              <Label for="file">Select File</Label>
              <Input type="file" name="file"></Input>
            </FormGroup>
            <FormGroup>
              <Button color="primary">Reserve</Button>{' '}
              <Button color="secondary" onClick={toggle}>Cancel</Button>
            </FormGroup>
          </Form>
        </ModalBody>
      </Modal>
    </div>
  );
}

export default Reserve;