import React, { Component} from 'react';
import {Nav, Navbar, NavbarBrand, Jumbotron, NavItem, Collapse, NavbarToggler, Button, Modal, ModalBody, ModalHeader,
        Form, FormGroup, Label, Input} from 'reactstrap';
import Reserve from './ReserveComponent';
import {NavLink} from 'react-router-dom';

class Header extends Component {
    
    constructor(props){
        super(props)
        this.state={
            isNavOpen:false,
            isLoginModalOpen:false
        };
      //  this.toggleNav = this.toggleNav.bind(this); use arraow function so we don't bind(this)
       // this.toggleLoginModal = this.toggleLoginModal.bind(this);
      //  this.handleLogin = this.handleLogin.bind(this);
    }
    toggleNav = () => {
        this.setState({
            isNavOpen:!this.state.isNavOpen
        });
    }
    toggleLoginModal = () =>{
        this.setState({
            isLoginModalOpen:!this.state.isLoginModalOpen
        });
    }
    handleLogin = (event) => {
        this.toggleLoginModal();
        alert("Username: " + this.username.value + " Password: " + this.password.value + " Remember me: " + this.remember.checked);
        event.preventDefault();
    }

    render(){
        return (
            <React.Fragment>
                <Navbar dark expand="md">
                    <div className="container">
                        <NavbarToggler onClick={this.toggleNav}></NavbarToggler>
                        <NavbarBrand className="mr-auto" href="/">
                            <img src="assets/images/logo.png" height="30" width="41" alt="Ristorante con Fusion"></img>
                        </NavbarBrand>
                        <Collapse isOpen ={this.state.isNavOpen} navbar>
                            <Nav navbar>
                                <NavItem>
                                    <NavLink className="nav-link"  to='/home'><span className="fa fa-home fa-lg"></span> Home</NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink className="nav-link" to='/aboutus'><span className="fa fa-info fa-lg"></span> About Us</NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink className="nav-link"  to='/menu'><span className="fa fa-list fa-lg"></span> Menu</NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink className="nav-link" to='/contactus'><span className="fa fa-address-card fa-lg"></span> Contact Us</NavLink>
                                </NavItem>
                            </Nav>
                            <Nav className ="ml-auto" navbar>
                                <NavItem>
                                    <Button outline onClick={this.toggleLoginModal}><span className="fa fa-sign-in fa-lg"></span>Login</Button>
                                </NavItem>
                            </Nav>
                        </Collapse>

                    </div>
                </Navbar>
                <Jumbotron>
                    <div className="container">
                        <div className ="row row-header">
                            <div className="col-12 col-sm-6">
                                <h1>Ristorante con Fusion</h1>
                                <p>We take inspiration from the World's best cuisines, and create a unique fusion experience. Our lipsmacking creations will tickle your culinary senses!</p>
                            </div>
                            <div className="col-12 col-sm align-self-center">
                                <img src="assets/images/logo.png" className="img-fluid" alt ="assets/images/logo.png"></img>
                            </div>
                            <div className="col-12 col-sm align-self-center">
                                <Reserve />
                            </div>
                        </div>
                    </div>
                </Jumbotron>
                <Modal isOpen={this.state.isLoginModalOpen} toggle={this.toggleLoginModal}>
                    <ModalHeader toggle={this.toggleLoginModal}>Login</ModalHeader>
                    <ModalBody>
                        <Form onSubmit={this.handleLogin}>
                            <FormGroup>
                                <Label>User Name</Label>
                                <Input type="text" id="username" name="username"
                                innerRef={(input) => this.username = input}></Input>
                            </FormGroup>
                            <FormGroup>
                                <Label>Password</Label>
                                <Input type="password" id="password" name="password"
                                innerRef={(input) => this.password = input}></Input>
                            </FormGroup>
                            <FormGroup check>
                                <Label check>
                                    <Input type="checkbox" name="remember"
                                    innerRef={(input) => this.remember = input}></Input>
                                    Remember me
                                </Label>
                            </FormGroup>
                            <FormGroup>
                                <Button type="submit" value="submit" color="primary">Login</Button>{' '}
                                <Button color="secondary" onClick={this.toggleLoginModal}>Cancel</Button>
                            </FormGroup>
                        </Form>
                    </ModalBody>
                </Modal>
            </React.Fragment>
        );
    }
}

export default Header;