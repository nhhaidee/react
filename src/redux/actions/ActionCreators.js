import * as ReduxActionTypes from './ActionTypes';
import {baseURL} from '../../shared/baseURL';

////////////////////////// DISHES ///////////////////////////////////////////
export const dishesLoadingCreators = () => ({
    type: ReduxActionTypes.DISHES_LOADING
});

export const dishesFailedCreators = (errmess) =>({
    type: ReduxActionTypes.DISHES_FAILED,
    payload: errmess
});

export const addDishesCreators = (dishes) => ({
    type: ReduxActionTypes.ADD_DISHES,
    payload: dishes
});

export const fetchDishesCreators = () => async (dispatch) => {

    dispatch(dishesLoadingCreators(true));
    return fetch(baseURL+'dishes')
        .then(response => {
            if (response.ok){
                return response;
            }
            else {
                var error = new Error('error' + response.status + ':' + response.statusText);
                error.response = response;
                throw error;
            }
        },
            error => {
                var errmess = new Error(error.message);
                throw errmess;
        })
        .then(response => response.json())
        .then(dishes => dispatch(addDishesCreators(dishes)))
        .catch(error => dispatch(dishesFailedCreators(error.message)));

};
/////////////////////////////////// END DISHES////////////////////////////////////

/////////////////////////////// PROMOTIONS //////////////////////////////////////

export const promosLoadingCreators = () => ({
    type: ReduxActionTypes.PROMOS_LOADING
});

export const promosFailedCreators = (errmess) =>({
    type: ReduxActionTypes.PROMOS_FAILED,
    payload: errmess
});

export const addPromosCreators = (promotions) => ({
    type: ReduxActionTypes.ADD_PROMOS,
    payload: promotions
});

export const fetchPromosCreators = () => async (dispatch) => {
    dispatch(promosLoadingCreators(true));
    return fetch(baseURL+'promotions')
        .then(response => {
            if (response.ok){
                return response;
            }
            else {
                var error = new Error('error' + response.status + ':' + response.statusText);
                error.response = response;
                throw error;
            }
        },
            error => {
                var errmess = new Error(error.message);
                throw errmess;
        })
        .then(response => response.json())
        .then(promotions => dispatch(addPromosCreators(promotions)))
        .catch(error => dispatch(promosFailedCreators(error.message)))
};

//////////////////////////// END PROMOTIONS ///////////////////////////////////////


////////////////////////////// COMMENTS ///////////////////////////////////////////
export const fetchCommentsCreator = () => async (dispatch) => {    
    return fetch(baseURL + 'comments')
    .then(response => response.json())
    .then(comments => dispatch(addComments(comments)));
};

export const commentsFailedCreator = (errmess) => ({
    type: ReduxActionTypes.COMMENTS_FAILED,
    payload: errmess
});

export const addComments = (comments) => ({
    type: ReduxActionTypes.ADD_COMMENTS,
    payload: comments
});

export const addCommentCreators = (comment) => ({
    type: ReduxActionTypes.USER_ADD_COMMENT,
    payload: comment
});

export const postCommentCreators = (dishId, rating, author, comment) => async (dispatch) => {

    const newComment = {
        dishId: dishId,
        rating: rating,
        author: author,
        comment: comment
    };

    newComment.date = new Date().toISOString();

    return fetch (baseURL +'comments', { 
        method:'POST', 
        body : JSON.stringify(newComment), 
        headers: {'Content-Type': 'application/json'}, 
        credentials:'same-origin'
    })
        .then (response => {
            if (response.ok){
                return response
            }
            else{
                var error = new Error('Error'+response.status+':'+response.statusText);
                error.response = response;
                throw error;
            }
        }, 
        error => {
            throw error;
        })
        .then(response => response.json())
        .then (response => dispatch(addCommentCreators(response)))
        .catch(error =>  { console.log('post comments', error.message); alert('Your comment could not be posted\nError: '+error.message); });
};
//////////////////////////////////END COMMENTS ///////////////////////////////////////

////////////////////////// LEADERS ///////////////////////////////////////////
export const leadersLoadingCreators = () => ({
    type: ReduxActionTypes.LEADERS_LOADING
});

export const leadersFailedCreators = (errmess) =>({
    type: ReduxActionTypes.LEADERS_FAILED,
    payload: errmess
});

export const addLeadersCreators = (leaders) => ({
    type: ReduxActionTypes.ADD_LEADERS,
    payload: leaders
});

export const fetchLeadersCreators = () => async (dispatch) => {

    dispatch(leadersLoadingCreators(true));
    return fetch(baseURL+'leaders')
        .then(response => {
            if (response.ok){
                return response;
            }
            else {
                var error = new Error('error' + response.status + ':' + response.statusText);
                error.response = response;
                throw error;
            }
        },
            error => {
                var errmess = new Error(error.message);
                throw errmess;
        })
        .then(response => response.json())
        .then(leaders => dispatch(addLeadersCreators(leaders)))
        .catch(error => dispatch(leadersFailedCreators(error.message)))

};
/////////////////////////////////// END LEADER////////////////////////////////////

export const postFeedbackCreators = (value) => {
    let newValue= {...value}     
    newValue.date=new Date().toISOString();
    return fetch(baseURL+'feedback',
    {
        method:'POST',
        body:JSON.stringify(newValue),
        headers: {
                'Content-Type':'application/json'
        },
        credentials:'same-origin'
    })

        .then(response=>{
            if (response.ok)
            {
                return response;
            }
            else
            {
                let error = new Error('Error '+response.status+": "+response.statusText);
                error.response = response;
                throw error;
            }
        },
        error=>{
            let errmess = new Error(error.message);
            throw errmess;
        })
        .then(response=>response.json())
        .then(response=>alert(JSON.stringify(response)))
        .catch(error=>{console.log('Post comments ',error.message);
                alert('Your Feedback could not be submitted\nError: '+error.message)
    });

}