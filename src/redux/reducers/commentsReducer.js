import * as ReduxActionsTypes from '../actions/ActionTypes';

export const CommentsReducer = (state = {errMess:null, commentsState:[]}, action) => {
    switch(action.type){

        case ReduxActionsTypes.ADD_COMMENTS:
            return {...state, errMess:null, commentsState:action.payload};

        case ReduxActionsTypes.COMMENTS_FAILED:
            return {...state, errMess: action.payload};

        case ReduxActionsTypes.USER_ADD_COMMENT:
            var comment = action.payload;
            return {...state, commentsState:  state.commentsState.concat(comment)};
        default:
            return state;
    }
}

