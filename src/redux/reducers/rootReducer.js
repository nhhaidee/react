import {combineReducers} from 'redux';
import {DishesReducer} from './dishesReducer';
import {CommentsReducer} from './commentsReducer';
import {PromotionsReducer} from './promotionsReducer';
import {LeadersReducer} from './leadersReducer';
import {InitialFeedback} from './formsReducer';
import {createForms} from 'react-redux-form';


export const rootReducer = combineReducers({
    dishesStore:DishesReducer,
    commentsStore:CommentsReducer,
    promotionsStore:PromotionsReducer,
    leadersStore: LeadersReducer,
    ...createForms({feedback:InitialFeedback})
});