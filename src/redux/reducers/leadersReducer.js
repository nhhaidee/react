import * as ReduxActionTypes from '../actions/ActionTypes';


export const LeadersReducer = (state = { isLoading:true, errMess:null, leadersState:[] }, action) => {

    switch(action.type)
    {
        case ReduxActionTypes.ADD_LEADERS:
            return{...state, isLoading: false, errMess: null, leadersState: action.payload}

        case ReduxActionTypes.LEADERS_LOADING:
            return{...state, isLoading: true, errMess: null, leadersState: []}

        case ReduxActionTypes.LEADERS_FAILED:
            return{...state, isLoading: false, errMess: action.payload, leadersState: []}

        default:
            return state;
    }
}