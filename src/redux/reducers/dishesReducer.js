import * as ReduxActionTypes from '../actions/ActionTypes';
//individual reducers
export const DishesReducer = (state = { isLoading:true, errMess:null, dishesState:[] }, action) => {

    switch(action.type)
    {
        case ReduxActionTypes.ADD_DISHES:
            return{...state, isLoading: false, errMess: null, dishesState: action.payload}

        case ReduxActionTypes.DISHES_LOADING:
            return{...state, isLoading: true, errMess: null, dishesState: []}

        case ReduxActionTypes.DISHES_FAILED:
            return{...state, isLoading: false, errMess: action.payload, dishesState: []}

        default:
            return state;
    }
}

