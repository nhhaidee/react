import * as ReduxActionTypes from '../actions/ActionTypes';

export const PromotionsReducer = (state  = { isLoading: true, errMess: null, promotionsState:[]}, action) => {
    
    switch (action.type) {
    case ReduxActionTypes.ADD_PROMOS:
        return {...state, isLoading: false, errMess: null, promotionsState: action.payload};

    case ReduxActionTypes.PROMOS_LOADING:
        return {...state, isLoading: true, errMess: null, promotionsState: []}

    case ReduxActionTypes.PROMOS_FAILED:
        return {...state, isLoading: false, errMess: action.payload};

    default:
        return state;
    }
};