import React, { Component } from 'react';
import Main from './components/MainComponent'
import {BrowserRouter} from 'react-router-dom';
import {Provider} from 'react-redux';
import {ConfigureReduxStore} from './redux/store/configureStore';

const ReduxStore = ConfigureReduxStore();
class App extends Component {
  
  render(){
    return (
      <Provider store={ReduxStore}>
        <BrowserRouter>
            <div>
              <Main />
            </div>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
